<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="br.com.projeto.model.User"  %>
<%@ page import="br.com.projeto.controller.UserController"  %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="shortcut icon" type="image/x-png" href="img/logo.png">
	<title>Tela de registro</title>
</head>

<body>
       <section class="container">
	   <div class="div-form">
		    <form id="form-adiciona" action="index.jsp" method="post">
		        <div class="grupo">
		            <input id="cpf" name="cpf" type="text"  placeholder="digite seu cpf(opcional)" class="campo">
		        </div>
		        <div class="grupo">
		            <input id="nome" name="nome" type="text" placeholder="digite o seu nome" class="campo">
		        </div>
		        <div class="grupo">
		            <input id="email" name="email" type="text" placeholder="digite o seu email" class="campo">
		        </div>
		        <div class="grupo">
		            <input id="senha" name="senha" type="password" placeholder="digite a sua senha" class="campo">
		        </div>

		        <button id="adicionar-usuario" value="Enviar" type="submit">Registrar</button>
		    </form>
		</section>
       </div>
       <script type="text/javascript" >
       var botao = document.querySelector("#adicionar-usuario");

		botao.addEventListener("click", function aoClicar() {
			 
			 <%
			 
			    String cpf = request.getParameter("cpf");
			    String nome = request.getParameter("nome");
			    String email = request.getParameter("email");
			    String senha = request.getParameter("senha");
			   
			    User user = new User();
			    
			    user.setCpf(cpf);
			    user.setNome(nome);
			    user.setEmail(email);
			    user.setSenha(senha);
			    
			   try{
				 
				   if(nome.length() > 0 
				    		&& senha.length() > 0 
				    		&& email.length() > 0  
				    		&& cpf.length() > 0){
				    
				    	try{
				    		UserController userElement = new UserController();
						    
						    userElement.salvar(user);
				    	} catch(Exception e){
				    		e.printStackTrace();
				    	}
				    	
				    } else{
				    	
				    }
				   
			   } catch(NullPointerException e) {
				  		   
			   } catch(Exception e) {
				   e.printStackTrace();
				   
			   }
			    
			 %>
		});
       </script>
</body>
</html>