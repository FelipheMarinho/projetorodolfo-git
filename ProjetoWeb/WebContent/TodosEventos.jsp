<%@page import="java.util.List"%>
<%@page import="br.com.projeto.services.ProjetoWebServices"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" type="text/css" href="css/all.css">
		<title>All events</title>
	</head>
	<header>
	</header>
	<body>
	 <div id="divSombra" class="invisivel">
        <div id="divParticipar" class="invisivel">
          <div class="divInformacoes">
                <img id="imagem" src=""/>
          </div>
          <div class="divInformacoes" id="participacao">
              <button id="btnSair" onClick="sair()">x</button>
              <ul>
                  <li>
                      <label for="email">Email:</label>
                      <input id="emailParticipante" name="email" placeholder="ex@gmail.com" type="text">
                  </li>
              </ul>
              <h6>Ou</h6>
              <button id="btnLonginParticipar">Login</button>
               <div class="divAdicionais">
                <p class="adicionais" id="pLocal"></p>
                <p class="adicionais" id="pContato"></p>
               </div>
              <button id="btnParticipar">Participar</button>
          </div>
        </div>
    </div>
	
		<input id="nomeEvento" name="nome" placeholder="Nome do Evento" type="text" oninput="filtrar(this)">
		<div id="divFiltro">
			<select id="cidade" name="cidade" class="campo" onchange="changeOption(this)">
				<option value="todas">Todas</option>
				<option value="recife">Recife</option>
				<option value="jaboatao">Jaboatao dos Guararapes</option>
				<option value="santacruz">Santa Cruz do Capibaribe</option>
				<option value="caruaru">Caruaru</option>
				<option value="olinda">Olinda</option>
				<option value="paulista">Paulista</option>
				<option value="petrolina">Petrolina</option>
			</select>
		</div>
		<div id="container">
		</div>
	</body>
	<script src="js/all.js"></script>
	<script src="js/participar.js"></script>
	<script type="text/javascript">
		
		function retornaEventos() {
			<%
			String features = "";
			try{
				List<String> eventFeatures = ProjetoWebServices.retornaEventos();
				features = eventFeatures.toString();
				
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			%>	
		}
		
		try {
			
			var array = '<%=features%>';
			var arrayEventos = array.split(",");
			
	        trazendoParamsJSP(arrayEventos);
	
			
		} catch (e) {
			console.log(e);
		}
		
	</script>
</html>