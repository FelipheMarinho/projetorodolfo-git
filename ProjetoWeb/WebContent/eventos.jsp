<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="br.com.projeto.services.ProjetoWebServices"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="shortcut icon" type="image/x-png" href="img/logo.png">
	<title>EVENTOS</title>
</head>

<body>
<section class="container">
	<div class="div-form">
		<form id="form" action="eventos.jsp" method="post" >
			<div class="grupo">
		      <input id="nome" name="nome" placeholder="Nome do Evento" type="text" class="campo">
			</div>
		    <div class="grupo">
		      <select id="cidade" name="cidade" class="campo">
					  <option value="recife">Recife</option>
					  <option value="santacruz">Santa Cruz do Capibaribe</option>
					  <option value="jaboatao">Jaboatao dos Guararapes</option>
					  <option value="caruaru">Caruaru</option>
					  <option value="olinda">Olinda</option>
					  <option value="paulista">Paulista</option>
					  <option value="petrolina">Petrolina</option>
				</select>
			</div>
			<div class="grupo">
		      <input id="local" name="local" placeholder="Ex.: Em frente ao restaurante man� matuto" type="text" class="campo">
			</div>
		    <div>
		      <input id="date" name="date" type="Date">
		      <input id="hour" name="hour" type="Time">
			</div>
			<div class="grupo">
		      <input id="contato" name="contato" placeholder="Ex.: (81)98888-8888" type="text" class="campo">
			</div>
			<div class="grupo">
			 <input type="file" id="input" onchange="handleFiles(this.files)"/>
			</div>
		    <div class="grupo">
		     <textarea id="dsEvento" name="dsEvento" placeholder="descreva como ser� o seu evento (opcional)" class="campo"></textarea>
			</div>
			<div class="grupo">
			<button type="submit" value="Enviar" id="botao">Cadastrar</button>
			</div>
			<input type="hidden" id="recebe" name="inputRecebe" value="" />   
		</form>
    </div>
</section>
<script type="text/javascript" >
 
var botao = document.querySelector("#botao");

botao.addEventListener("click", function aoClicar() {
	

	 <% 
	  String date = request.getParameter("date");
	  String horario = request.getParameter("hour");
	  String nome = request.getParameter("nome");
	  String contato = request.getParameter("contato");
	  String cidade = request.getParameter("cidade");
	  String localidade = request.getParameter("local");
	  String base64 = request.getParameter("inputRecebe");
	  String ds_evento = request.getParameter("dsEvento");
	 
	 try{
		 
			 ProjetoWebServices.tratarParametrosEvento(date, horario, localidade, cidade, base64, ds_evento, nome, contato);
		 		 
	 }catch(Exception e ) {
		 e.printStackTrace();
	 }
	  
	  
	  %>
});

</script>
<script src="js/mainEvento.js"></script>
</body>
</html>