function exibirEventos(data, hora, imagem, nome, cidade, local, contato){
    //criando elementos
    var divDentro = document.createElement("div");
    divDentro.setAttribute('onClick','participar(this)');
    var container = document.querySelector("#container");
    var div = document.createElement("DIV");
    var img = document.createElement("IMG");
    
    let aux = contato;

    var localidade = document.createElement("input");
    localidade.setAttribute('type','hidden');
    localidade.setAttribute('id','localEvento');
    localidade.textContent = local;

    var contato = document.createElement("input");
    contato.setAttribute('type','hidden');
    contato.setAttribute('id','contatoEvento');
    contato.textContent = aux;
    
    var nomeEvento = document.createElement("H3");
    nomeEvento.classList.add("NomeEvento");
    nomeEvento.textContent = nome;
    var horario = document.createElement("p");
    horario.classList.add("DataHorario");
    horario.textContent = data + " - " + hora;
    var local = document.createElement("p");
    local.classList.add("Cidade");
    local.textContent = cidade.toUpperCase() + "- PE";
    img.src = imagem;
    div.classList.add("DivDeFora");
    divDentro.classList.add("DivDeDentro");
    divDentro.appendChild(img);
    divDentro.appendChild(nomeEvento);
    divDentro.appendChild(horario);
    divDentro.appendChild(local);
    divDentro.appendChild(localidade);
    divDentro.appendChild(contato);
    div.appendChild(divDentro);
    container.appendChild(div);
    
}

var container = document.querySelector("#container");

container.addEventListener("mouseover", function () {

    var classeDoElemento = event.target.parentNode.classList.value;

    if(classeDoElemento == "DivDeDentro"){
        event.target.parentNode.classList.add("Sombra");
    }
    
});

container.addEventListener("mouseout", function () {

    var classeDoElemento = event.target.parentNode.classList.value;

    if(classeDoElemento != "DivDeDentro"){
        event.target.parentNode.classList.remove("Sombra");
    }
});

function trazendoParamsJSP(arrayEventos){
    try {
        console.log(arrayEventos.length);
    let i = 0;
     
    for (; i < arrayEventos.length-8; i++) {
        
        var data = arrayEventos[i];
        if(i == 0){
           data = arrayEventos[i].substring(1,11);
        }
        var hora = arrayEventos[i + 1];
        var imagem = arrayEventos[i + 2] + "," + arrayEventos[i + 3];
        var nome = arrayEventos[i + 4];
        var cidade = arrayEventos[i + 5];
        var local = arrayEventos[i + 6];
        var contato = arrayEventos[i + 7];
        
        exibirEventos(data, hora, imagem, nome, cidade, local, contato);	

        i = i + 7;
    }

        var data = arrayEventos[i];
        var hora = arrayEventos[i + 1];
        var imagem = arrayEventos[i + 2] + "," + arrayEventos[i + 3];
        var nome = arrayEventos[i + 4];
        var cidade = arrayEventos[i + 5];
        var local = arrayEventos[i + 6];
        var contato = arrayEventos[i + 7];
        
        exibirEventos(data, hora, imagem, nome, cidade, local, contato.substring(0, cidade.length - 1));	

    } catch (error) {
     console.log(error);   
    }
}

//#f5f5f5 <-- cor antiga do background

function changeOption(opcao){

	  var divsDeFora = document.querySelectorAll(".DivDeFora");

	  var expReg = new RegExp(opcao.value, "i");

	  divsDeFora.forEach(function(elemento){
	    
	    var texto = elemento.textContent.toLowerCase();
	    var cidade = texto.split('-');

	   if(expReg.test('todas')){
	      elemento.classList.remove("invisivel"); 
	    }else{
	            if(expReg.test(cidade[1])){
	              elemento.classList.remove("invisivel");
	            }else{
	              elemento.classList.add("invisivel");
	            }
	    }
	  });

	}

function filtrar(opcao){
 	
 	var divsDeFora = document.querySelectorAll(".DivDeFora");
  	var expReg = new RegExp(opcao.value, "i");

   divsDeFora.forEach(function(elemento){
	
	var textoNome = elemento.textContent.toLowerCase();

	var nome = textoNome.split('-');


	if(expReg.test(nome[0])){
		elemento.classList.remove('invisivel');
	}else{
		elemento.classList.add('invisivel');
	}

   });


}

function participar(este) {

	var divSombra = document.querySelector('#divSombra');
    var divParticipar = document.querySelector('#divParticipar');
    
    
     var imagem =  este.querySelector('img');

     var imgParticipa = divParticipar.querySelector('img');
     imgParticipa.src = imagem.src;
    
     let plocal = este.querySelector('#localEvento'); 
     let pcontato = este.querySelector('#contatoEvento'); 
        
     let localDiv = document.querySelector('#pLocal');
     localDiv.textContent ='Local: ' + plocal.textContent;
     let contatoDiv = document.querySelector('#pContato');
     contatoDiv.textContent ='Contato: ' + pcontato.textContent;

    divSombra.classList.remove('invisivel');
    divParticipar.classList.remove('invisivel');
}