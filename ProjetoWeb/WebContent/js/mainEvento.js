function handleFiles(files) {
		
    try {
        for (var i = 0; i < files.length; i++) {
             var file = files[i];
             var imageType = /image.*/;
         
             if (!file.type.match(imageType)) { continue; }
         
             var reader = new FileReader();
             reader.readAsDataURL(file);
                
             
             console.log(reader.result);
              
             reader.onload = function () { 
                 console.log(reader.result); 
                 document.querySelector("#recebe").value = reader.result;
                 };   
             reader.onerror = function (error) { console.log('Error: ', error); };
              
          }
    } catch (e) {
        console.log(e);
    }
     
}