package br.com.projeto.model;


import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "EVENTOS")
public class Eventos {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(columnDefinition = "LONGBLOB")
	private byte[] foto;
	@Temporal(TemporalType.DATE)
	private Date data;
	@Column(name = "horario")
	private String hora;
	@Column(name="tp_evento")
	private String tipo;
	@Column(name="ds_evento")
	private String dsEvento;
	@Column(name="localidade")
	private String local;
	@Column(name="nm_evento")
	private String nm_evento;
	@Column(name="contato")
	private String contato;
	@Column(name="cidade")
	private String cidade;
	@ManyToMany
	private List<User> participantes;
	
	
	public String getNm_evento() {
		return nm_evento;
	}

	public void setNm_evento(String nm_evento) {
		this.nm_evento = nm_evento;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}
	
	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDsEvento() {
		return dsEvento;
	}

	public void setDsEvento(String dsEvento) {
		this.dsEvento = dsEvento;
	}

	public int getId() {
		return id;
	}
	
	public byte[] getFoto() {
		return foto;
	}
	
	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	
	
	
	
	
}
