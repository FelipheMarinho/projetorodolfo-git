package br.com.projeto.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import br.com.projeto.controller.EventosController;
import br.com.projeto.model.Eventos;

public class ProjetoWebServices {
	
	public static void main(String[] args) {
		
		Eventos evento = new Eventos();
		EventosController controller = new EventosController();
		
	}

	public static String dateToString(Date data) {
		
		String retorno = null;
		
		String[] date = data.toString().split("-");
		retorno = date[2] + "/" + date[1] + "/" + date[0];

		return retorno;
	}

	public static String formatData(String date){
		if (date == null || date.equals(""))
			return null;
		    String realDate = "";
		try {
			String[] vetor = date.split("-");
			realDate = vetor[2] + "/" + vetor[1] + "/" + vetor[0];
		} catch (ParseException e) {
			throw e;
		}
		return realDate;
	}
	
	public static Date formatarData(String date) throws Exception {
		if (date == null || date.equals(""))
			return null;
		Date data = null;
		try {
			String[] vetor = date.split("-");
			String realDate = vetor[2] + "/" + vetor[1] + "/" + vetor[0];
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			data = (Date) formatter.parse(realDate);
		} catch (ParseException e) {
			throw e;
		}
		return data;
	}

	public static void tratarParametrosEvento(String date, String horario, String localidade, String cidade,
			String base64, String ds_evento, String nome, String contato) {

		EventosController dao = new EventosController();
		Eventos evento = new Eventos();

		if (isNotNull(date)) {
			try {
				Date data = formatarData(date);
				evento.setData(data);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (isNotNull(horario)) {
			evento.setHora(horario);
		}
		if (isNotNull(localidade)) {
			evento.setLocal(localidade);
		}
		if (isNotNull(cidade)) {
			evento.setCidade(cidade);
		}
		if (isNotNull(base64)) {
			evento.setFoto(base64ToArraysByte(base64));
		}
		if (isNotNull(ds_evento)) {
			evento.setDsEvento(ds_evento);
		}
		if (isNotNull(nome)) {
			evento.setNm_evento(nome);
		}
		if (isNotNull(contato)) {
			evento.setContato(contato);
		}

		try {
			if (isNotNull(localidade) && isNotNull(horario)) {
				dao.salvar(evento);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static String base64ToString(String base64){
		String retorno = null;
		
		try {
			
			byte[] arrayBytes = base64ToArraysByte(base64);
			retorno = new String(arrayBytes);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return retorno;
	}
	
	public static List<String> retornaEventos(){
		List<Eventos> selecionados = new ArrayList<>();
		List<String> carcEventos = new ArrayList<>();
		String base64 = "";
		try {
			EventosController eventElement = new EventosController();
			
			selecionados = eventElement.findAllEvents();
			for (int i = 0;i < selecionados.size(); i++) {
				carcEventos.add(ProjetoWebServices.formatData(selecionados.get(i).getData().toString()));
				carcEventos.add(selecionados.get(i).getHora());
				base64 = ProjetoWebServices.imgByteToBase64(selecionados.get(i).getFoto()).replaceAll("[\r\n]+", " ");
				carcEventos.add(base64);
				carcEventos.add(selecionados.get(i).getNm_evento());
				carcEventos.add(selecionados.get(i).getCidade());
				
				carcEventos.add(selecionados.get(i).getLocal());
				carcEventos.add(selecionados.get(i).getContato());
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return carcEventos;
	}
	
	
	public static ArrayList<String> metodoAllEvent(String p_tp_evento){
		ArrayList<String> selecionados = new ArrayList<>();
		try {
			
			
			if(isNotNull(p_tp_evento)){
							
		        EventosController eventElement = new EventosController();
		                   
		        List<Eventos> eventos = eventElement.findType(p_tp_evento);
		        
		        String base64 = ProjetoWebServices.imgByteToBase64(eventos.get(0).getFoto());
		        base64 = base64.replaceAll("[\r\n]+", " ");
		        
		        String date = ProjetoWebServices.dateToString(eventos.get(0).getData());
		        String hora = eventos.get(0).getHora();
		        
		        
		        selecionados.add(base64);
		        selecionados.add(date);
		        selecionados.add(hora);
		        	
			}else{
				selecionados.add("null");
				selecionados.add("null");
				selecionados.add("null");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		        
        return selecionados;
	}
	

	public static byte[] base64ToArraysByte(String base64) {

		byte[] buf = new byte[] { 0x12, 0x23 };

		try {

			buf = new sun.misc.BASE64Decoder().decodeBuffer(base64);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return buf;
	}
	
	public static String imgByteToBase64(byte[] img) {

		String imgBase64BD = "";
		String aux = "";
		String data = "data";

		try {

			imgBase64BD = new sun.misc.BASE64Encoder().encode(img);

			if (imgBase64BD.substring(0, 4).equals(data)) {

				aux = imgBase64BD.substring(0, 22);
				String[] array = aux.split("/");
				aux = array[0] + ":" + array[1] + "/" + array[2] + ";" + array[3] + ",";
				imgBase64BD = aux + imgBase64BD.substring(22, imgBase64BD.length());

			} else {
				imgBase64BD = "data:image/png;base64," + imgBase64BD;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return imgBase64BD;

	}
	

	public static String imgBancoToBase64(EventosController eventDao, int id) {

		String imgBase64BD = "";
		byte[] img = null;
		String aux = "";
		String data = "data";

		try {

			img = eventDao.findFotoById(id);
			imgBase64BD = new sun.misc.BASE64Encoder().encode(img);

			if (imgBase64BD.substring(0, 4).equals(data)) {

				aux = imgBase64BD.substring(0, 22);
				String[] array = aux.split("/");
				aux = array[0] + ":" + array[1] + "/" + array[2] + ";" + array[3] + ",";
				imgBase64BD = aux + imgBase64BD.substring(22, imgBase64BD.length());

			} else {
				imgBase64BD = "data:image/png;base64," + imgBase64BD;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return imgBase64BD;

	}

	public static boolean isNull(Object v) {
		return v == null;
	}

	public static boolean isNotNull(Object v) {
		return v != null;
	}

	// public static void uploadImg(EventoController eventDao, Evento event,
	// String path){
	//
	// BufferedImage imagem;
	//
	// try {
	//
	// imagem = ImageIO.read(new File(path));
	// ByteArrayOutputStream bytesImg = new ByteArrayOutputStream();
	// ImageIO.write((BufferedImage)imagem, "jpg", bytesImg);//seta a imagem
	// para bytesImg
	// bytesImg.flush();//limpa a vari�vel
	// byte[] byteArray = bytesImg.toByteArray();//Converte
	// ByteArrayOutputStream para byte[]
	// bytesImg.close();//fecha a convers�o
	//
	//
	// event.setFoto(byteArray);
	// eventDao.salvar(event);
	//
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	//
	// }

//	public static byte[] redimencionandoImagem(byte[] imageByte) throws IOException{
//		BufferedImage imgResized = new BufferedImage(336, 384, BufferedImage.TYPE_INT_RGB);
//		
//		try {
//			InputStream inputImage = new ByteArrayInputStream(imageByte);
//			BufferedImage image = ImageIO.read(inputImage);
//	        imgResized.getGraphics().drawImage(image, 0, 0, 336, 384, null);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		 return imageToByte(imgResized);
//	}
//	
//	private static byte[] imageToByte(BufferedImage img) throws IOException {
//	    try (ByteArrayOutputStream baos = new ByteArrayOutputStream();) {
//	        ImageIO.write(img, "PNG", baos);
//	        baos.flush();
//	        return baos.toByteArray();
//	    }
//	}
	
}
