package br.com.projeto.controller;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.projeto.model.User;

public class UserController {
	

	EntityManagerFactory emf;
	EntityManager em;
	
	public UserController(){
		emf = Persistence.createEntityManagerFactory("unidade");
		em = emf.createEntityManager();
	}
	
	public void salvar(User user){
		
		try {
			em.getTransaction().begin();
			em.merge(user);
			em.getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			emf.close();	
		}
		
	}
	
}
