package br.com.projeto.controller;


import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.com.projeto.model.Eventos;
import br.com.projeto.services.ProjetoWebServices;

public class EventosController {

	
	EntityManagerFactory emf;
	EntityManager em;
	
	public EventosController(){
		emf = Persistence.createEntityManagerFactory("unidade");
		em = emf.createEntityManager();
	}
	
    public void salvar(Eventos eventos){
		
		try {
			em.getTransaction().begin();
			em.merge(eventos);
			em.getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			emf.close();	
		}
		
	}
    
    public byte[] findFotoById(int id){
    	 List<Eventos> eventos = new LinkedList<>();
    	
    	try {
    		
    		em.getTransaction().begin();
        	Query query = em.createQuery("from Eventos where id = :id ");
        	query.setParameter("id", id);
            eventos = query.getResultList();
            
		} catch (Exception e) {
             e.printStackTrace();
		}
        
    	return eventos.get(0).getFoto();
    }
    
    public List<Eventos> findType(String p_tp_evento){
    	
    	List<Eventos> eventos = new LinkedList<>();
    	
    	try {
    		em.getTransaction().begin();
        	Query query = em.createQuery("from Eventos where tp_evento = :p_tp_evento ");
        	query.setParameter("p_tp_evento", p_tp_evento);
            eventos = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	
    	return eventos;    	
    }
    
    public static void main(String[] args){
    	
    	EventosController controller = new EventosController();
    	
    	List<Eventos> eventos =  controller.findAllEvents();
    	
    	
    	for (int i = 0; i < eventos.size(); i++) {
			System.out.println(eventos.get(i).getId());
			System.out.println(eventos.get(i).getLocal());
			System.out.println(eventos.get(i).getTipo());
			System.out.println(eventos.get(i).getData());
			System.out.println(ProjetoWebServices.imgByteToBase64(eventos.get(i).getFoto()));
			System.out.println(eventos.get(i).getDsEvento());
			System.out.println();
		}
    	
    }
    
    
    public List<Eventos> findAllEvents(){
    	
    
	List<Eventos> eventos = new LinkedList<>();
    	
    	try {
    		em.getTransaction().begin();
        	Query query = em.createQuery("from Eventos ");
            eventos = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	
    	return eventos;
    	
    }
	
}
